import Vue from "vue"
import VueRouter from "vue-router"

import allLogin from "@/views/auth/allLogin"
import allRegister from "@/views/auth/allRegister"
import pwRetrieval from "@/views/auth/pwRetrieval"
import tenantTable from "@/views/tenant/tenantTable"
import tenantForm from "@/views/tenant/tenantForm"
import userTable from "@/views/user/userTable"
import userForm from "@/views/user/userForm"
import replierTable from "@/views/replier/replierTable"
import replierForm from "@/views/replier/replierForm"
import groupTable from "@/views/group/groupTable"
import groupForm from "@/views/group/groupForm"


import surveyTable from "@/views/survey/surveyTable"
import surveyDesigner from "@/views/survey/surveyDesigner"
import surveyPreview from "@/views/survey/surveyPreview"
import surveySender from "@/views/survey/surveySender"
import surveyAnswer from "@/views/survey/surveyAnswer"
import surveyResultTable from "@/views/survey/surveyResultTable"
import surveyAnalysis from "@/views/survey/surveyAnalysis"
import surveyProjectTable from "@/views/survey/surveyProjectTable"
import surveyCreation from "@/views/survey/surveyCreation"
import surveyLinkTable from "@/views/survey/surveyLinkTable"
import surveyForm from "@/views/survey/surveyForm"
import store from "../store"


Vue.use(VueRouter)
//TODO: 优化为更restful的风格
const router =new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/allLogin',
            component: allLogin,
        },{
            path: '/allRegister',
            component: allRegister
        },{
            path: '/pwRetrieval',
            component: pwRetrieval
        },
        {
            path: '/tenantTable',
            component: tenantTable,
        },
        {
            path: '/tenantForm',
            component: tenantForm,
        },{
            path: '/userTable',
            component: userTable,
        },
        {
            path: '/userForm',
            component: userForm,
        },{
            path:'/groupForm',
            component:groupForm,
        },{
            path: '/groupTable',
            component:groupTable,
        },{
            path: '/replierTable',
            component:replierTable,
        },{
            path: '/replierForm',
            component:replierForm,
        },
        {
            path: '/surveyTable/',
            component: surveyTable,
        },
        {
            path: '/surveyAnalysis',
            component: surveyAnalysis,
        },
        {
            path: '/surveyDesigner/:purpose',
            component: surveyDesigner,
        },
        {
            path:'/surveyPreview',
            component: surveyPreview
        },
        {
            path: '/surveySender',
            component: surveySender,
        }, 
        {
            path: '/surveyAnswer',
            component:surveyAnswer,
        },
        {
            path: '/surveyResultTable',
            component: surveyResultTable,
        },
        {
            path: '/surveyProjectTable',
            component: surveyProjectTable,
        },{
            path:'/surveyCreation',
            component: surveyCreation
        },{
            path: '/surveyLinkTable',
            component: surveyLinkTable
        },{
            path:'/surveyForm',
            component: surveyForm
        },
       
        {
            path: '/',
            redirect: '/allLogin'
        }
    ]
})


router.beforeEach((to, from, next) => {
    let authed = true;
    if(store.state.userToken == ''||store.state.userToken == null){
        authed = false;
    }
    if (to.path !== '/allLogin' && !authed) next({ path: '/allLogin' })
    else next()
  })

  export default router;